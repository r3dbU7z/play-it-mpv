# Mpv + youtube-dl + Firefox = ♥
```diff
-=Try don't laugh at the code, PLS=-
```

This is firefox browser extension that gives the user the ability to play video streams for [multiple](http://rg3.github.io/youtube-dl/supportedsites.html) websites using [youtube-dl](http://youtube-dl.org) and a native media player [mpv](http://mpv.io/). Here's is the list of all the [supported sites](http://rg3.github.io/youtube-dl/supportedsites.html).
   
## Installing

### Build
1. Download artifacts.zip from the [pipelines page/Download build artifacts](https://gitlab.com/r3dbU7z/play-it-mpv/pipelines)
2. Open the `play-it-mpv.xpi` file with Firefox. It is recommended to use [Waterfox Classic](https://www.waterfox.net/download/)(no digital signature error, **this add-on is NOT signed**) 
3. Tested on versions: Waterfox 2020.05 (64-bit) and below. Yeap, Tor Browser aka FF `v.52.0.6` also fit (check `certificate validation: xpinstall.signatures.required`).
3. Or use the load through `about: debugging` tab.

### Build it manually
1. Download the source.
2. Download the [Add-on SDK](https://developer.mozilla.org/en-US/Add-ons/SDK/Tools/jpm#Installation).
3. Run `jpm xpi` inside the `project` directory.
4. Open the `play-it-mpv.xpi` file with Firefox.

### Requirements

* MPV Player - download page: [https://mpv.io/installation/](https://mpv.io/installation/) 

* Youtube-dl Downloader - download page: [https://rg3.github.io/youtube-dl/download.html](https://rg3.github.io/youtube-dl/download.html)

### WARNING

This add-on doesn't work in Firefox 57 (Quantum and newer), since the add-on uses the old SDK API not compatible with WebExtensions.

### Add-ons with FF57 support

1. [external-video](https://addons.mozilla.org/en-US/firefox/addon/external-video/) -- by @vayan [GitHub](https://github.com/vayan/external-video)
2. [ff2mpv](https://addons.mozilla.org/en-US/firefox/addon/ff2mpv/) -- by @woodruffw [GitHub](https://github.com/woodruffw/ff2mpv)
3. [yt2p](https://addons.mozilla.org/en-US/firefox/addon/yt2p/) -- by @sumzary [GitHub](https://github.com/Sumzary/yt2p)
4. And etc. (see also google.com)

### Inspired by
Original [mpv-youtube-dl-binding](https://addons.mozilla.org/ru/firefox/addon/watch-with-mpv/) repository by @antoniy: [GitHub](https://github.com/antoniy/mpv-youtube-dl-binding)

See also [WIKI-fork](https://gitlab.com/r3dbU7z/play-it-mpv/wikis/home)

### MPV logo

Author: [Chris Ward](http://tenzerothree.com/) GitHub - [@tenzerothree](https://github.com/tenzerothree)

```diff
+Rockin' In The Free World+
```